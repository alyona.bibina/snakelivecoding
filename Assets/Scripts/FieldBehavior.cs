using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldBehavior : MonoBehaviour
{
    [SerializeField]
    int width = 9;
    [SerializeField]
    int height = 9;
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = new Vector3(width, height, 1);
    }

    public bool IsOutBound(Vector3 pos)
    {
        if (pos.x <  -width / 2 || pos.x > width / 2 ) {
            return true;
        }

        if (pos.y < -height / 2 || pos.y > height / 2) {
            return true;
        }

        return false;
    }

    public Vector3 GetRandomCell()
    {
        var x = Random.Range(0, width);
        var y = Random.Range(0, height);

        return new Vector3(x - width * 0.5f + 0.5f , y - height * 0.5f + 0.5f, 0);
    }
}
