using System.Collections.Generic;
using UnityEngine;


public interface IFruitFollower
{
    void OnFruitDestroy();
}

public class FruitBehaviour : MonoBehaviour
{
    [SerializeField]
    SnakeBehavior snake;

    List<IFruitFollower> followers = new List<IFruitFollower>();

    private void Start()
    {
        snake = FindObjectOfType<SnakeBehavior>();
    }

    public void Subcribe(IFruitFollower fruitFollower)
    {
        followers.Add(fruitFollower);
    }

    public void UnSubscribe(IFruitFollower fruitFollower)
    {
        followers.Remove(fruitFollower);
    }


    private void OnDestroy()
    {
        if (followers.Count > 0) {
            foreach (var f in followers) {
                f?.OnFruitDestroy();
            }
        }

        followers.Clear();
    }

    private void FixedUpdate()
    {
        if (snake.GetPos() == transform.position) {
            snake.GrowUp();
            Destroy(gameObject);
        }
    }

    


}
