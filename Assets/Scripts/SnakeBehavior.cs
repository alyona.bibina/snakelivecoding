using System.Collections.Generic;
using UnityEngine;

public class SnakeBodyPart
{
    public GameObject part;
    public Vector2 dir;
}


public class SnakeBehavior : MonoBehaviour
{
    [SerializeField]
    FieldBehavior field;

    [SerializeField]
    float speed = 5;

    [SerializeField]
    GameObject snakePiece;

    [SerializeField]
    int startPieceCount = 3;

    Vector2 dir = Vector2.zero;

    float nextMove = 0;

    bool dead = false;

    List<SnakeBodyPart> body = new List<SnakeBodyPart>();

    private void Start()
    {
        for(int i = 0; i < startPieceCount; i++) {
            var part = new SnakeBodyPart() { part = Instantiate(snakePiece, this.transform.position - Vector3.left * i, Quaternion.identity, this.transform), dir = Vector2.left };
            body.Add(part);
        }
    }

    private void FixedUpdate()
    {
        if (dead) {
            return;
        }

        if (dir.sqrMagnitude > 0) {
            if (Time.time > nextMove) {
                var nextPos = body[0].part.transform.position + new Vector3(body[0].dir.x, body[0].dir.y, 0);

                if (field.IsOutBound(nextPos)) {
                    Die();
                } else {
                    List<int> turns = new List<int>();
                    body[0].part.transform.Translate(body[0].dir);
                    for(int i = 1; i < body.Count; i++) {
                        body[i].part.transform.Translate(body[i].dir);
                        if (body[i].dir != body[i- 1].dir) {
                            turns.Add(i);
                        }
                    }

                    turns.Reverse();

                    foreach(var idx in turns) {
                        body[idx].dir = body[idx - 1].dir;
                    }

                    CheckSnakeHasStuck();

                    nextMove = Time.time + 1 / speed;
                }

                
            }
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) && dir != Vector2.left && dir != Vector2.right) {
            dir = Vector2.left;            
        } else if (Input.GetKeyDown(KeyCode.RightArrow) && dir != Vector2.left && dir != Vector2.right) {
            dir = Vector2.right;
        } else if (Input.GetKeyDown(KeyCode.UpArrow) && dir != Vector2.up && dir != Vector2.down) {
            dir = Vector2.up;
        } else if (Input.GetKeyDown(KeyCode.DownArrow) && dir != Vector2.up && dir != Vector2.down) {
            dir = Vector2.down;
        }

        body[0].dir = dir;
    }
    public void GrowUp()
    {
        var lastDir = body[body.Count - 1].dir;
        var part = new SnakeBodyPart() { part = Instantiate(snakePiece, body[body.Count - 1].part.transform.position - 
            new Vector3(lastDir.x, lastDir.y, 0), Quaternion.identity, this.transform), dir = lastDir };
        body.Add(part);
    }

    public Vector3 GetPos()
    {
        return body[0].part.transform.position;
    }

    public void CheckSnakeHasStuck()
    {
        for(int i = 1; i < body.Count; i++) {
            if (body[i].part.transform.position == body[0].part.transform.position) {
                Die();
                break;
            }
        }
    }

    void Die()
    {
        dead = true;
        foreach (var piece in body) {
            piece.part.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    public bool IsInside(Vector3 pos)
    {
        for (int i = 0; i < body.Count; i++) {
            if (body[i].part.transform.position == pos) {
                return true;
            }
        }

        return false;
    }

}
