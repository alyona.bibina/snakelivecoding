using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitGenerator : MonoBehaviour, IFruitFollower
{
    [SerializeField]
    GameObject fruitTemplate;

    [SerializeField]
    FieldBehavior field;

    FruitBehaviour curFruit;
    SnakeBehavior snake;

    void Start()
    {
        snake = FindObjectOfType<SnakeBehavior>();
        GenerateFruit();
    }

    private void OnDestroy()
    {
        curFruit?.UnSubscribe(this);
    }


    void GenerateFruit()
    {
        var cell = field.GetRandomCell();
        while( snake.IsInside(cell) ) {
            cell = field.GetRandomCell();
        }
        var fruit = Instantiate(fruitTemplate, cell, Quaternion.identity);
        curFruit = fruit.GetComponent<FruitBehaviour>();
        curFruit.Subcribe(this);
    }

    public void OnFruitDestroy()
    {
        GenerateFruit();
    }
}
